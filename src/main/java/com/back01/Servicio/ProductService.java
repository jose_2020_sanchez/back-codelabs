package com.back01.Servicio;


import com.back01.modelo.ProductoModel;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private final AtomicLong secuenciador = new AtomicLong(0);
    public final ConcurrentHashMap<Long, ProductoModel> productos = new ConcurrentHashMap<Long, ProductoModel>();

    public ProductoModel getProducto(long id){
        final ProductoModel p = this.productos.get(id);
        return p;
    }

    public List<ProductoModel> getProductos() {
        return this.productos
                .entrySet()
                .stream()
                .map(x -> x.getValue())
                .collect(Collectors.toList());
    }


    public  long  addProducto(ProductoModel p){
        p.id = this.secuenciador.incrementAndGet();
        this.productos.put(p.id,p);
        return p.id;
    }


    public void updateProducto(long id, ProductoModel p){
        p.id = id;
        this.productos.put(id, p);
    }


    public void removeProducto(long id){
        this.productos.remove(id);
    }

}
