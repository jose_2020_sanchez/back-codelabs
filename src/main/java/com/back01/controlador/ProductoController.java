package com.back01.controlador;

import com.back01.Servicio.ProductService;
import com.back01.modelo.ProductoModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductoController {

    @Autowired
    ProductService productService;


    @GetMapping("/productos/{id}")
    public ResponseEntity getProductoId(@PathVariable long id) {
        final ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }

    @PostMapping("/productos")
    public ResponseEntity<String> addProducto(@RequestBody ProductoModel productoModel) {
        productService.addProducto(productoModel);
        return new ResponseEntity<>("Product created successfully!", HttpStatus.CREATED);
    }

    @GetMapping("/productos")
    public List<ProductoModel> getProductosId() {
        return this.productService.getProductos();
    }


    @PutMapping("/productos/{id}")
    public ResponseEntity updateProducto(@PathVariable long id,
                                         @RequestBody ProductoModel productToUpdate) {
        ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        this.productService.updateProducto(id, productToUpdate);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK); // Buenas prácticas dicen que mejor enviar 204 No Content
    }


    @DeleteMapping("/productos/{id}")
    public ResponseEntity deleteProducto(@PathVariable Long id) {
        ProductoModel pr = productService.getProducto(id);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        this.productService.removeProducto(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT); // status code 204-No Content
    }

    @PatchMapping("/productos/{id}")
    public ResponseEntity patchPrecioProducto(
            @RequestBody ProductoModel productoModel, @PathVariable long id){

        final ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        this.productService.updateProducto(id, productoModel);
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }


    @GetMapping("/productos/{id}/users")
    public ResponseEntity getProductIdUsers(@PathVariable long id){
        final ProductoModel pr = productService.getProducto(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (pr.users!=null)
            return ResponseEntity.ok(pr.users);
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
